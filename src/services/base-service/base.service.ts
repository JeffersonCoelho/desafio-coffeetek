import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { URL_PRODUCTS } from "../../utils/constants/constants";

@Injectable()
export class BaseService {
  protected baseUrl: string = URL_PRODUCTS;
  protected timeout: number = 5000;
  requestOptions;
  constructor(public http: HttpClient) { }

  async configRequestOptions() {
    this.requestOptions = {
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      params: new HttpParams(),
      responseType: 'json'
    }
    return this.requestOptions;
  }

  protected get(endpoint?: string) {
    return new Promise((resolve, reject) => {
      this.http.get(this.baseUrl + endpoint, this.requestOptions)
        .timeout(this.timeout)
        .subscribe(
          res => {
            console.log('res', res)
            resolve(res);
          }, error => {
            console.log('res', error)
            reject(error);
          }
        )

    });
  }

  protected post(endpoint: string, body: any) {
    return new Promise((resolve, reject) => {
      this.http.post(this.baseUrl + endpoint, body)
        .timeout(this.timeout)
        .subscribe(
          res => {
            resolve(res);
          }, error => {
            reject(error);
          }
        )
    });
  }

  protected put(endpoint: string, body: any) {
    return new Promise((resolve, reject) => {
      this.http.put(this.baseUrl + endpoint, body)
        .timeout(this.timeout)
        .subscribe(
          res => {
            resolve(res);
          }, error => {
            reject(error);
          }
        )
    });
  }

  protected delete(endpoint: string) {
    return new Promise((resolve, reject) => {
      this.http.delete(this.baseUrl + endpoint)
        .timeout(this.timeout)
        .subscribe(
          res => {
            resolve(res);
          }, error => {
            reject(error);
          }
        )
    });
  }
}
