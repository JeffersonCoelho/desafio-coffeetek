import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "../base-service/base.service";

@Injectable()
export class ProductService extends BaseService {

  constructor(public http: HttpClient) {
    super(http);
  }

  async getProducts() {
    await this.configRequestOptions();
    return new Promise((resolve, reject) => {
      this.get('')
        .then((res: any) => {
          resolve(res)
        })
        .catch(err => reject(err));
    });
  }
}
