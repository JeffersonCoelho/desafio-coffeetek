import { NgModule } from '@angular/core';
import { CustomNavbarComponent } from './custom-header/custom-navbar';
import { IonicPageModule } from 'ionic-angular';
@NgModule({
	declarations: [CustomNavbarComponent],
	imports: [
		IonicPageModule.forChild(CustomNavbarComponent)
	],
	exports: [CustomNavbarComponent]
})
export class ComponentsModule { }
