import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'custom-navbar',
  templateUrl: 'custom-navbar.html'
})
export class CustomNavbarComponent {

  @Input() title: string;

  constructor(public navCtrl: NavController) {
  }

  backPage() {
    this.navCtrl.pop();
  }
}
