import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";

const CART_KEY = 'my-cart';

@Injectable()
export class CartStorage {

  constructor(public storage: Storage) { }

  public saveCart(value: any): Promise<any> {
    return this.storage.get(CART_KEY).then((cart: any[]) => {

      if (cart) {
        cart.forEach(element => {
          if (element.product.title === value.product.title) {
            value.quantity += element.quantity;
            this.updateCart(value);
            return;
          }
        });

        cart.push(value);

        return this.storage.set(CART_KEY, cart);
      } else {
        return this.storage.set(CART_KEY, [value]);
      }
    });
  }

  updateCart(value: any): Promise<any> {
    return this.storage.get(CART_KEY).then((carts: any[]) => {
      if (!carts || carts.length === 0) {
        return null;
      }
      let listAux: any[] = [];
      for (let cart of carts) {
        if (cart.product.title === value.product.title) {
          listAux.push(value);
        } else {
          listAux.push(cart);
        }
      }

      return this.storage.set(CART_KEY, listAux);
    });
  }

  public removeCart(value: any): Promise<any> {
    return this.storage.get(CART_KEY).then((carts: any[]) => {
      if (!carts || carts.length === 0) {
        return null;
      }

      let listAux: any[] = [];
      for (let cart of carts) {
        if (cart.product.title !== value.product.title) {
          listAux.push(cart);
        }
      }

      return this.storage.set(CART_KEY, listAux);
    });
  }

  public getCarts(): Promise<any[]> {
    return this.storage.get(CART_KEY);
  }
}