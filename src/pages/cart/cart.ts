import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { CartStorage } from '../../storage/storage-cart';

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {
  itemsCart: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public cartStorage: CartStorage, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    this.getStorage();
  }

  removeProduct(itemCart: any) {
    const alert = this.alertCtrl.create({
      title: 'Atenção!',
      message: "Você deseja apagar este item do carrinho?",
      buttons: [
        {
          text: 'Sim',
          handler: data => {
            this.cartStorage.removeCart(itemCart)
              .then(result => { this.getStorage(); })
              .catch(error => { console.log('Error =>', error) });
          }
        },
        {
          text: 'Não',
          handler: data => {
          }
        }
      ]
    });
    alert.present();
  }

  formatterAdditionals(additionals: Array<string>): string {
    let newAdditionals: string;

    newAdditionals = additionals.toString();
    return this.replaceAll(newAdditionals, ',', '<br>');
  }

  private async getStorage() {
    await this.cartStorage.getCarts()
      .then((result) => { this.itemsCart = result; })
      .catch(error => { alert('Ocorreu um erro ao tentar pegar os itens do carrinho, tente novamente!' + `\n${error}`) });
  }

  private replaceAll(text: string, searchValue: string, replacer: string) {
    var pos = text.indexOf(searchValue);
    while (pos > -1) {
      text = text.replace(searchValue, replacer);
      pos = text.indexOf(searchValue);
    }
    return text;
  }
}
