import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProductService } from '../../services/product/product.service';
import { PRODUCTSJSON } from '../../utils/mock';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  products: any;
  constructor(public navCtrl: NavController, private productsService: ProductService) { }

  ionViewDidLoad() {
    // this.getProducts();
    this.products = PRODUCTSJSON.products;
    console.log
  }

  private async getProducts() {
    this.products = await this.productsService.getProducts().then(result => {
      console.log('result', result);
    }).catch(error => {
      console.log('error', error);
    });
    console.log('this.products', this.products);
  }

  goDetails(product) {
    this.navCtrl.push('DetailsPage', { product: product });
  }
  
  goCart() {
    this.navCtrl.push('CartPage');
  }
}
