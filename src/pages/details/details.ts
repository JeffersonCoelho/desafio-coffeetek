import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { CartStorage } from '../../storage/storage-cart';

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
  product: any;
  sizes: Array<number> = [0, 1, 2];
  sugars: Array<any> = [{ iconName: 'custom-no-sugar' }, { iconName: 'custom-sugar1' }, { iconName: 'custom-sugar2' }, { iconName: 'custom-sugar3' }];
  additionals: Array<string>;
  quantity: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public cartStorage: CartStorage) {

    this.product = this.navParams.get('product');
    this.additionals = this.product.additional;
  }

  ionViewWillEnter() {
    if (!this.product) {
      alert('Ocorreu um problema!\nDesculpe o transtorno.');
      this.navCtrl.setRoot(HomePage, { animate: false });
    }
  }

  lessQuantity() {
    if (this.quantity > 0) {
      this.quantity--;
    }
  }

  plusQuantity() {
    this.quantity++;
  }

  goCart() {
    let infoCart = { product: this.product, quantity: this.quantity };
    this.setStorage(infoCart);
  }

  private async setStorage(infoCart: any) {
    await this.cartStorage.saveCart(infoCart)
      .then(result => { this.navCtrl.push('CartPage'); })
      .catch(error => { alert('Ocorreu um erro ao tentar salvar o item no carrinho, tente novamente!' + `\n${error}`) });
  }
}
